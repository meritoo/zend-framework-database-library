<?php

/**
 * Base class for model
 *
 * @author Krzysztof Niziol <krzysztof.niziol@meritoo.pl>
 * @copyright Meritoo.pl
 */
abstract class Meritoo_Model_Abstract {

    /**
     * Class constructor
     *
     * @param array | Zend_Db_Table_Row $data Table's data
     * @return void
     */
    public function __construct($data = null) {
        $this->update($data);
    }

    /**
     * Returns value for given property
     *
     * @param string $name Name of the property
     * @return mixed
     */
    public function __get($name) {
        $name = lcfirst($name);

        if (isset($this->{$name})) {
            return $this->{$name};
        }

        return null;
    }

    /**
     * Sets value for given property
     *
     * @param string $name Name of the property
     * @param mixed $value Value of the property
     * @return Application_Model_Abstract
     */
    public function __set($name, $value) {
        $this->{$name} = $value;
        return $this;
    }

    /**
     * Calls proper method
     *
     * @param string $name Name of the method / function
     * @param mixed $arguments Arguments of the method / function
     * @return mixed
     */
    public function __call($name, $arguments) {
        $methodPattern = '/(get|set)([a-zA-Z0-9]+)/';

        $matches = array();
        $matchCount = preg_match($methodPattern, $name, $matches);

        if ($matchCount > 0) {
            $name = lcfirst($matches[2]);
            $type = $matches[1];

            if ($type == 'get') {
                return $this->__get($name);
            } elseif ($type == 'set') {
                $value = $arguments[0];
                return $this->__set($name, $value);
            }
        }
    }

    /**
     * Returns data of the model as an array with database fields names as keys and values as... values :)
     * @return array
     */
    public function toArray() {
        $effect = array();
        $properties = get_object_vars($this);

        if (!empty($properties)) {
            foreach ($properties as $property => $value) {
                $property = ucfirst($property);
                $pattern = '|[A-Z]{1}[a-z0-9]+|';

                $matches = array();
                $matchCount = preg_match_all($pattern, $property, $matches);

                if ($matchCount > 0) {
                    $parts = $matches[0];
                    $fieldName = strtolower(implode('_', $parts));

                    $effect[$fieldName] = $value;
                }
            }
        }

        return $effect;
    }

    /**
     * Updates data of the object
     * 
     * @param array $data The data
     * @return void
     */
    public function update($data) {
        if ($data !== null) {
            if ($data instanceof Zend_Db_Table_Row) {
                $data = $data->toArray();
            }

            if (!empty($data)) {
                foreach ($data as $column => $value) {
                    $column = str_replace('_', ' ', $column);
                    $column = ucwords($column);

                    $column = str_replace(' ', '', $column);
                    $column = lcfirst($column);

                    $this->{$column} = $value;
                }
            }
        }
    }

}