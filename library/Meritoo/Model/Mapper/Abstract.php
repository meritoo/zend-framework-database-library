<?php

/**
 * Base class for mapper of model
 *
 * @author Krzysztof Niziol <krzysztof.niziol@meritoo.pl>
 * @copyright Meritoo.pl
 */
abstract class Meritoo_Model_Mapper_Abstract {

    /**
     * The database table
     * @var \Meritoo_Db_Table_Abstract
     */
    private $dbTable = null;

    /**
     * Class constructor
     *
     * @param [mixed $dbTable = null] The databse table name or object's reference
     * @return void
     */
    abstract public function __construct($dbTable = null);

    /**
     * Returns database table
     * @return \Meritoo_Db_Table_Abstract
     */
    public function getDbTable() {
        return $this->dbTable;
    }

    /**
     * Sets the database table
     *
     * @param mixed $dbTable The database table
     * @return \Meritoo_Model_Mapper_Abstract
     */
    public function setDbTable($dbTable) {
        if (is_string($dbTable)) {
            $dbTable = new $dbTable();
        }

        if (!$dbTable instanceof \Meritoo_Db_Table_Abstract) {
            throw new Zend_Exception('Invalid table data gateway provided');
        }

        $this->dbTable = $dbTable;
        return $this;
    }

    /**
     * Returns data for the select field (form's element)
     *
     * @param [array $where = array()] Parameters for the WHERE clause
     * @param [array $orderBy = array()] Parameters for the ORDER BY clause
     * @param [array $columns = array()] Columns' names used to get values for the select field. It has to be two columns.
     * @param [array $defaultValue = array()] The default dat (id and name), e.g. array(0 => '-- All --')
     * @return array
     */
    public function getForSelectField($where = array(), $orderBy = array(), $columns = array(), $defaultValue = array()) {
        $effect = array();
        $table = $this->getDbTable();

        if (!empty($columns) && count($columns) != 2) {
            throw new Zend_Exception('The columns amount cannot be different than 2');
        }

        // Setting default columns' names
        if (empty($columns)) {
            $columns = array('id', 'name');
        }

        // Simple, basic select
        $select = $table->select()
                ->from($table->getName(), $columns);

        // Setting the WHERE clause
        if (!empty($where)) {
            Meritoo_Db_Table_Utilities::addWhereClauses($select, $where);
        }

        // Setting the ORDER BY clause
        if (!empty($orderBy)) {
            Meritoo_Db_Table_Utilities::addOrderByClauses($select, $orderBy);
        }

        //echo($select);die;
        $rowset = $table->fetchAll($select);

        if ($rowset !== null && $rowset->count() > 0) {
            $idColumn = $columns[0];
            $nameColumn = $columns[1];

            if (!empty($defaultValue) && is_array($defaultValue)) {
                $effect = $defaultValue;
            }

            foreach ($rowset as $row) {
                $id = trim($row->{$idColumn});
                $name = trim($row->{$nameColumn});

                $effect[$id] = $name;
            }
        }

        return $effect;
    }

    /**
     * Saves data of given object.
     * When no 'id' property is set or is null, INSERT is make. Otherwise - UPDATE.
     *
     * @param mixed $data The data to save
     * @return boolean | integer
     */
    public function save($data) {
        $effect = null;

        if (is_object($data) && $data instanceof stdClass) {
            $data = get_object_vars($data);
        } elseif (is_object($data) && $data instanceof Application_Model_Abstract) {
            $data = $data->toArray();
        } elseif (!is_array($data)) {
            $data = array();
        }

        if (!empty($data)) {
            if ($this->beforeSave($data)) {
                if (!isset($data['id']) || $data['id'] === null || (int) $data['id'] == 0) {
                    if (isset($data['id'])) {
                        unset($data['id']);
                    }

                    $effect = (int) $this->getDbTable()->insert($data);
                } else {
                    $id = (int) $data['id'];

                    if ($id > 0) {
                        unset($data['id']);
                        $effect = (boolean) $this->getDbTable()->update($data, array('id = ?' => $id));
                    }
                }
            }
        }

        return $effect;
    }

    /**
     * Deletes object of given ID
     *
     * @param integer $id The object ID
     * @return boolean
     */
    public function delete($id) {
        if ($this->beforeDelete($id)) {
            return (boolean) $this->getDbTable()->delete('id = ' . $id);
        }

        return null;
    }

    /**
     * Returns values of given column
     *
     * @param [array $where = array()] Parameters for the WHERE clause
     * @param [string $column = 'id'] Name of the column. By default name of the primary key column is used.
     * @return array
     */
    public function getColumnValues($where = array(), $column = 'id') {
        $effect = array();
        $table = $this->getDbTable();

        $select = $table->select()
                ->distinct(true)
                ->from(
                $table, $column
        );

        // Setting the WHERE clause
        if (!empty($where)) {
            Meritoo_Db_Table_Utilities::addWhereClauses($select, $where);
        }

        //echo($select);
        $rowset = $table->fetchAll($select);

        if ($rowset !== null && $rowset->count() > 0) {
            foreach ($rowset as $row) {
                $value = $row->{$column};
                $effect[] = $value;
            }
        }

        return $effect;
    }

    /**
     * Executes some functionality before saving object's data
     * 
     * @param array $data The object's data
     * @return boolean
     */
    protected function beforeSave(&$data) {
        return true;
    }

    /**
     * Executes some functionality before deleting object's data
     * 
     * @param integer $objectId Deleted object's ID
     * @return boolean
     */
    protected function beforeDelete($objectId) {
        return true;
    }

}