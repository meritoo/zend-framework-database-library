<?php

/**
 * Class for SQL SELECT query manipulation for the Zend_Db_Table component.
 * In case of use deleted_at and updated_at columns this class extends the Zend_Db_Table_Select class and overrides some important methods.
 * 
 * @author Krzysztof Niziol <krzysztof.niziol@meritoo.pl>
 * @copyright Meritoo.pl
 */
class Meritoo_Db_Table_Select extends Zend_Db_Table_Select {

    /**
     * The database table
     * @var Meritoo_Db_Table_Abstract
     */
    private $dbTable = null;

    /**
     * Returns the database table
     * @return Meritoo_Db_Table_Abstract
     */
    public function getDbTable() {
        return $this->dbTable;
    }

    /**
     * Sets the database table
     * 
     * @param Meritoo_Db_Table_Abstract $dbTable The database table
     * @return \Meritoo_Db_Table_Select
     */
    public function setDbTable($dbTable) {
        $this->dbTable = $dbTable;
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function from($name, $cols = self::SQL_WILDCARD, $schema = null) {
        $this->includeDeletedAtFilter($name);
        return parent::from($name, $cols, $schema);
    }

    /**
     * {@inheritdoc}
     */
    protected function _join($type, $name, $cond, $cols, $schema = null) {
        $this->includeDeletedAtFilter($name, $cond);
        return parent::_join($type, $name, $cond, $cols, $schema);
    }

    /**
     * Includes the "deleted at" filter which is used for rows marked as deleted
     * 
     * @param string $tableName Name of the table. It may be passed to the from() method.
     * @param [string $condition = ''] The condition used generally by the join*() methods
     * @return \Meritoo_Db_Table_Select
     */
    private function includeDeletedAtFilter($tableName, &$condition = '') {
        $alias = '';
        $table = $this->getDbTable();

        if (is_array($tableName)) {
            $key = key($tableName);
            $alias = sprintf('%s.', $key);
            $tableName = $tableName[$key];
        }

        if ($table === null || !empty($condition)) {
            $table = new Meritoo_Db_Table_Abstract();
            $table->setName($tableName);
        }

        if ($table->isDeletedAtUsed()) {
            $columnName = $table->getDeletedAtColumnName();
            $conditionPart = sprintf('%s%s IS NULL', $alias, $columnName);

            if (empty($condition)) {
                $this->where($conditionPart);
            } else {
                $condition .= sprintf(' AND %s', $conditionPart);
            }
        }

        return $this;
    }

}