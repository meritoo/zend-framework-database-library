<?php

/**
 * Some kind of utilities for database table
 *
 * @author Krzysztof Niziol <krzysztof.niziol@meritoo.pl>
 * @copyright Meritoo.pl
 */
class Meritoo_Db_Table_Utilities {

    /**
     * Adds / inserts the WHERE clauses into existing SQL statement.
     * Returns the SQL statement.
     *
     * @param Zend_Db_Select $select The SQL statement
     * @param array $where Conditions and values for the WHERE clauses
     * @return Zend_Db_Select
     */
    public static function addWhereClauses(Zend_Db_Select $select, array $where) {
        foreach ($where as $item) {
            $condition = $item[0];
            $value = null;
            $or = false;

            if (isset($item[1])) {
                $value = $item[1];
            }

            if (isset($item[2])) {
                if (strtoupper($item[2]) === Zend_Db_Select::SQL_OR) {
                    $or = true;
                }
            }

            if ($or) {
                $select->orWhere($condition, $value);
            } else {
                $select->where($condition, $value);
            }
        }

        return $select;
    }

    /**
     * Adds / inserts the ORDER BY clauses into existing SQL statement.
     * Returns the SQL statement.
     *
     * @param Zend_Db_Select $select The SQL statement
     * @param array $orderBy Field names with sort types for the ORDER BY clause
     * @return Zend_Db_Select
     */
    public static function addOrderByClauses(Zend_Db_Select $select, array $orderBy) {
        foreach ($orderBy as $fieldName) {
            $select->order($fieldName);
        }

        return $select;
    }

}