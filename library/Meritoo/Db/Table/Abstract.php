<?php

/**
 * Base class for database table
 *
 * @author Krzysztof Niziol <krzysztof.niziol@meritoo.pl>
 * @copyright Meritoo.pl
 */
class Meritoo_Db_Table_Abstract extends Zend_Db_Table_Abstract {

    /**
     * Name of the "deleted at" column used to mark deleted rows
     * @var string
     */
    private $deletedAtColumnName = 'deleted_at';

    /**
     * Name of the "updated at" column used to set last date of row's update
     * @var string
     */
    private $updatedAtColumnName = 'updated_at';

    /**
     * Information if the "deleted_at" column is used to mark deleted rows
     * @var boolean
     */
    private $isDeletedAtUsed = null;

    /**
     * Information if the "updated_at" column is used to set last date of row's update
     * @var boolean
     */
    private $isUpdatedAtUsed = null;

    /**
     * {@inheritdoc}
     * @throws \Exception
     */
    public function init() {
        parent::init();

        if (empty($this->_name)) {
            throw new Zend_Exception(sprintf('The class %s has not set the $_name property!', __CLASS__));
        }
    }

    /**
     * Returns the table name
     * @return string
     */
    public function getName() {
        return $this->_name;
    }

    /**
     * Sets the table name
     * 
     * @param string $name The table name
     * @return \Meritoo_Db_Table_Abstract
     */
    public function setName($name) {
        $this->_name = $name;
        return $this;
    }

    /**
     * Returns information if the "deleted_at" column is used to mark deleted rows
     * @return boolean
     */
    public function isDeletedAtUsed() {
        if ($this->isDeletedAtUsed === null) {
            $this->isDeletedAtUsed = $this->isColumnUsed($this->deletedAtColumnName);
        }

        return $this->isDeletedAtUsed;
    }

    /**
     * Returns information if the "updated_at" column is used to set last date of row's update
     * @return boolean
     */
    public function isUpdatedAtUsed() {
        if ($this->isUpdatedAtUsed === null) {
            $this->isUpdatedAtUsed = $this->isColumnUsed($this->updatedAtColumnName);
        }

        return $this->isUpdatedAtUsed;
    }

    /**
     * Returns name of the "deleted at" column used to mark deleted rows
     * @return string
     */
    public function getDeletedAtColumnName() {
        return $this->deletedAtColumnName;
    }

    /**
     * Returns name of the "updated at" column used to set last date of row's update
     * @return string
     */
    public function getUpdatedAtColumnName() {
        return $this->updatedAtColumnName;
    }

    /**
     * {@inheritdoc}
     */
    public function select($withFromPart = self::SELECT_WITHOUT_FROM_PART) {
        $select = new Meritoo_Db_Table_Select($this);

        if ($withFromPart === self::SELECT_WITH_FROM_PART) {
            $select->from($this->info(self::NAME), Meritoo_Db_Table_Select::SQL_WILDCARD, $this->info(self::SCHEMA));
        }

        $select->setDbTable($this);
        return $select;

        /*
         * Previous / old version
         * 
         * $select = parent::select($withFromPart);
         * return $this->includeDeletedAtFilter($select);
         */
    }

    /**
     * {@inheritdoc}
     */
    public function fetchAll($where = null, $order = null, $count = null, $offset = null) {
        $where = $this->includeDeletedAtFilter($where);
        return parent::fetchAll($where, $order, $count, $offset);
    }

    /**
     * {@inheritdoc}
     */
    public function fetchRow($where = null, $order = null, $offset = null) {
        $where = $this->includeDeletedAtFilter($where);
        return parent::fetchRow($where, $order, $offset);
    }

    /**
     * Truncates the table
     * @return \Zend_Db_Statement_Interface
     */
    public function truncate() {
        return $this->getAdapter()
                        ->query(sprintf('TRUNCATE TABLE %s', $this->_name));
    }

    /**
     * Returns / Fetches count of rows
     * 
     * @param string | array | Zend_Db_Table_Select $where An SQL WHERE clause or Zend_Db_Table_Select object
     * @param integer $count An SQL LIMIT count
     * @param integer $offset An SQL LIMIT offset
     * @return integer
     */
    public function fetchCount($where = null, $count = null, $offset = null) {
        $where = $this->includeDeletedAtFilter($where);

        $select = $this->select()
                ->from($this, array('rowsCount' => new Zend_Db_Expr('COUNT(*)')))
                ->where($where)
                ->limit($count, $offset);

        return (int) $this->getAdapter()
                        ->fetchOne($select);
    }

    /**
     * {@inheritdoc}
     * @return boolean
     */
    public function delete($where) {
        if ($this->isDeletedAtUsed()) {
            $data = array(
                $this->deletedAtColumnName => date('Y-m-d H:i:s')
            );

            return (boolean) parent::update($data, $where);
        }

        return (boolean) $this->delete($where);
    }

    /**
     * {@inheritdoc}
     * @return boolean
     */
    public function update(array $data, $where) {
        if ($this->isUpdatedAtUsed() && !isset($data[$this->updatedAtColumnName])) {
            $data[$this->updatedAtColumnName] = date('Y-m-d H:i:s');
        }

        return (boolean) parent::update($data, $where);
    }

    /**
     * Returns information if given column is used by table, if is one of table's columns
     * 
     * @param string $columnName Name of the column
     * @return boolean
     */
    public function isColumnUsed($columnName) {
        $columns = $this->_getCols();
        return in_array($columnName, $columns);
    }

    /**
     * Includes the "deleted at" filter which is used for rows marked as deleted
     * 
     * @param string | array $where An SQL WHERE clause
     * @return string | array
     */
    private function includeDeletedAtFilter($where) {
        if ($this->isDeletedAtUsed()) {
            $deletedAtCondition = sprintf('%s IS NULL', $this->getDeletedAtColumnName());

            if (is_array($where)) {
                $where[] = $deletedAtCondition;
            } elseif (is_string($where)) {
                if (!empty($where)) {
                    $where .= ' AND ';
                }

                $where .= $deletedAtCondition;
            }
        }

        return $where;
    }

}